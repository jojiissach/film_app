# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from main.models import BaseModel
from django.core.validators import MinValueValidator
from decimal import Decimal
from django.utils.translation import ugettext as _
from django.db.models import Sum


ACC_TYPE = (
    ('savings','Savings'),
    ('current','Current'),
)

TRANSACTION_CATEGORY_TYPES = (
    ('income','Income'),
    ('expense','Expense'),
)


class BankAccount(BaseModel):
    name = models.CharField(max_length=128)
    ifsc = models.CharField(max_length=128)
    branch = models.CharField(max_length=128)
    account_type = models.CharField(max_length=128,choices=ACC_TYPE) 
    account_no = models.CharField(max_length=128)
    first_time_balance = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))]) 
    balance = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    payumoney_email = models.EmailField()
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'finance_bank_account'
        verbose_name = _('bank account')
        verbose_name_plural = _('bank accounts')
        ordering = ('name',)
        
    def __unicode__(self):
        return self.name
    

class TransactionCategory(BaseModel):
    name = models.CharField(max_length=128)
    category_type = models.CharField(max_length=15,choices=TRANSACTION_CATEGORY_TYPES)
    is_system_generated = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'transaction_category'
        verbose_name = 'transaction category'
        verbose_name_plural = 'transaction categories'
        ordering = ('category_type',)
        
    def __unicode__(self):
        return self.name

        
class Transaction(BaseModel):
    transaction_type = models.CharField(max_length=15,choices=TRANSACTION_CATEGORY_TYPES) 
    transaction_category = models.ForeignKey("finance.TransactionCategory")   
    employer = models.ForeignKey("employers.Employer",blank=True,null=True)
    candidate = models.ForeignKey("candidates.Candidate",blank=True,null=True)
    cheque_details = models.CharField(max_length=128,null=True,blank=True)
    is_cheque_withdrawed = models.BooleanField(default=False)
    card_details = models.CharField(max_length=128,null=True,blank=True)
    bank_account = models.ForeignKey("finance.BankAccount",null=True,blank=True)
    amount = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])    
    time = models.DateTimeField() 
    
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'finance_transaction'
        verbose_name = 'transaction'
        verbose_name_plural = 'transactions'
        ordering = ('-time',)
        
    class Admin:
        list_display = ('id', 'amount')
        
    def __unicode__(self):
        return self.amount
