from django.conf.urls import url
import views


urlpatterns = [
 
    url(r'^$', views.dashboard,name='dashboard'), 

    url(r'^bank-account/create/$',views.create_bank_account,name='create_bank_account'),
    url(r'^bank-account/edit(?P<pk>.*)/$',views.edit_bank_account,name='edit_bank_account'),
    url(r'^bank-accounts/$',views.bank_accounts,name='bank_accounts'),
    url(r'^bank-account/(?P<pk>.*)/$',views.bank_account,name='bank_account'),

    url(r'^transactions/$',views.transactions,name='transactions'),
    url(r'^transaction/view/(?P<pk>.*)/$',views.transaction,name='transaction'),

]