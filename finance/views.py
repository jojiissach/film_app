from django.shortcuts import render, get_object_or_404
from main.decorators import  ajax_required, check_mode
from django.contrib.auth.decorators import login_required
from finance.forms import TransactionForm, BankAccountForm,\
    TransactionCategoryForm
from django.core.urlresolvers import reverse
from main.functions import generate_form_errors
from django.http.response import HttpResponse, HttpResponseRedirect
import json
from finance.models import TransactionCategory, Transaction, BankAccount
import datetime
from django.db.models import Q
from main.functions import get_auto_id
from main.decorators import check_mode,permissions_required,ajax_required,role_required
from django.core import serializers
from users.functions import get_current_role


@check_mode
@login_required
@role_required(['superadmin'])
def dashboard(request):
    return HttpResponseRedirect(reverse('finance:transactions'))
    

@check_mode
@login_required
@role_required(['superadmin'])
def create_bank_account(request):
 
    if request.method == "POST":
        response_data = {} 
        form = BankAccountForm(request.POST)
        
        if form.is_valid(): 
            paypal_email = form.cleaned_data['paypal_email']
            if not BankAccount.objects.filter(paypal_email=paypal_email).exists():
                first_balance = form.cleaned_data['first_time_balance']
                auto_id = get_auto_id(BankAccount)
                
                #create Bank Account
                data = form.save(commit=False)
                data.creator = request.user
                data.updator = request.user
                data.auto_id = auto_id
                data.balance = first_balance
                data.save()

                response_data = {
                    'status' : 'true',
                    'title' : "Successfully Created",
                    'redirect' : 'true',
                    'redirect_url' : reverse('finance:bank_account', kwargs = {'pk' : data.pk}),
                    'message' : 'Bank Account Successfully Created.',
                }

            else:  
                response_data = {
                    'status' : 'false',
                    'title' : "Form validation error",
                    'stable' : 'true',
                    'message' : "Bank Account Exists with this email.",
                }
        else:
            message = ''            
            message += generate_form_errors(form,formset=False)  
            response_data = {
                'status' : 'false',
                'title' : "Form validation error",
                'stable' : 'true',
                'message' : message,
            }
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = BankAccountForm()
        
        context = {
            "form" : form,
            "title" : "Create Bank Account",
            'redirect' : True,

            "is_need_popup_box": True,
            "is_need_grid_system": True,
            "is_need_animations": True,
            "is_need_datetime_picker": True,
            "is_need_checkbox": True,
            "is_need_select_picker" : True,
        }
        return render(request, 'finance/entry_bank_account.html', context)
  

@check_mode
@login_required
@role_required(['superadmin'])
def edit_bank_account(request,pk): 
    instance = get_object_or_404(BankAccount.objects.filter(pk=pk,is_deleted=False))  
    if request.method == "POST":
        form = BankAccountForm(request.POST,instance=instance)
        
        if form.is_valid(): 

            #update Bank Account
            first_balance = form.cleaned_data['first_time_balance']
            data = form.save(commit=False)
            data.updator = request.user
            data.balance = first_balance
            data.date_updated = datetime.datetime.now()
            data.save()
            response_data = {
                'status' : 'true',
                'title' : "Successfully Updated",
                'redirect' : 'true',
                'redirect_url' : reverse('finance:bank_account', kwargs = {'pk' : data.pk}),
                'message' : 'Bank Account Successfully Updated.',
            }
        else:
            message = ''            
            message += generate_form_errors(form,formset=False)  
            response_data = {
                'status' : 'false',
                'title' : "Form validation error",
                'stable' : 'true',
                'message' : message,
            }
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = BankAccountForm(instance=instance)
        
        context = {
            "form" : form,
            "title" : "Edit Bank Account : " + instance.name,
            "instance" : instance,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_grid_system": True,
            "is_need_animations": True,
            "is_need_datetime_picker": True,
            "is_need_checkbox": True,
            "is_need_select_picker" : True,
        }
        return render(request, 'finance/entry_bank_account.html', context)
  

@check_mode
@login_required
@role_required(['superadmin'])
def bank_accounts(request): 
    instances = BankAccount.objects.filter(is_deleted=False)
    
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query) | Q(account_no__icontains=query))
        
    title = "Bank Accounts"
    context = {
        'title' : title,
        "instances" : instances,

        "is_need_popup_box": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
        "is_need_checkbox": True,
        "is_need_select_picker" : True,
    }
    return render(request,'finance/bank_accounts.html',context) 


@check_mode
@login_required
@role_required(['superadmin'])
def bank_account(request,pk): 
    instance = get_object_or_404(BankAccount.objects.filter(pk=pk))
    context = {
        "instance" : instance,
        "title" : "Bank Account : " + instance.name,
        "single_page" : True,

        "is_need_popup_box": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
        "is_need_checkbox": True,
        "is_need_select_picker" : True,
        "is_need_formset" : True,
    }
    return render(request,'finance/bank_account.html',context)
    

@check_mode
@ajax_required
@login_required
@role_required(['superadmin'])
def delete_bank_account(request,pk): 
    instance = get_object_or_404(BankAccount.objects.filter(pk=pk))
    
    BankAccount.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted")
    
    response_data = {}
    response_data['status'] = 'true'        
    response_data['title'] = "Successfully Deleted"       
    response_data['redirect'] = 'true' 
    response_data['redirect_url'] = reverse('finance:bank_accounts')
    response_data['message'] = "Bank Account Successfully Deleted."
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def delete_selected_bank_accounts(request): 
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(BankAccount.objects.filter(pk=pk,is_deleted=False)) 
            BankAccount.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Bank Account(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('finance:bank_accounts')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some bank accounts first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def transactions(request): 
    instances = Transaction.objects.filter(is_deleted=False)
    
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(customer__name__icontains=query) | Q(amount__icontains=query))
        
    title = "Transactions"
    context = {
        'title' : title,
        "instances" : instances,
        "is_need_popup_box": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
        "is_need_checkbox": True,
        "is_need_select_picker" : True,
    }
    return render(request,'finance/transactions.html',context) 


@check_mode
@login_required
@role_required(['superadmin'])
def transaction(request,pk): 
    instance = get_object_or_404(Transaction.objects.filter(pk=pk))
    context = {
        "instance" : instance,
        "title" : "Transaction : " + str(instance.amount) + " - " + str(instance.time.date()),
        "single_page" : True,
        "is_need_popup_box": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
        "is_need_checkbox": True,
        "is_need_select_picker" : True,
    }
    return render(request,'finance/transaction.html',context)