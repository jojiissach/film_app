from __future__ import unicode_literals
from django.db import models
from decimal import Decimal
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from main.models import BaseModel


class Payment(BaseModel):
    candidate = models.ForeignKey('candidates.Candidate',blank=True,null=True)
    employer = models.ForeignKey('employers.Employer',blank=True,null=True)
    amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    currency = models.CharField(max_length=200,null=True,blank=True)
    language = models.CharField(max_length=200,null=True,blank=True)
    payment_type = models.CharField(max_length=200,null=True,blank=True)
    payment_type_instrument = models.CharField(max_length=500,null=True,blank=True)
    payment_mode = models.CharField(max_length=200,null=True,blank=True)
    card_name = models.CharField(max_length=200,null=True,blank=True)
    order_status = models.CharField(max_length=200,null=True,blank=True)
    transaction_id = models.CharField(max_length=200,null=True,blank=True)
    payment_id = models.CharField(max_length=200,null=True,blank=True)
    purpose = models.CharField(max_length=200,null=True,blank=True)
    failure_message = models.TextField(null=True,blank=True)
    status_message = models.TextField(null=True,blank=True)
    mode = models.CharField(max_length=200,null=True,blank=True)
    billing_name = models.CharField(max_length=200,null=True,blank=True)
    billing_address = models.TextField(null=True,blank=True)
    billing_tel = models.CharField(max_length=200,null=True,blank=True)
    billing_email = models.CharField(max_length=200,null=True,blank=True)

    bank_account = models.ForeignKey('finance.BankAccount',blank=True,null=True)
    is_registration = models.BooleanField(default=False)
    is_completed = models.BooleanField(default=False)
    is_promotion = models.BooleanField(default=False)
    class Meta:
        db_table = 'instamojo_payments'
        verbose_name = _('payment')
        verbose_name_plural = _('payments')
        ordering = ('-auto_id',)
  
    def __unicode__(self): 
        return "%s" %str(self.amount)