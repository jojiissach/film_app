from django import forms
from django.forms.widgets import TextInput

class PayForm(forms.Form):
    name = forms.CharField(widget=TextInput(attrs={'class': 'required form-control'}))
    email = forms.EmailField(widget=TextInput(attrs={'class': 'required form-control'}))
    phone = forms.CharField(widget=TextInput(attrs={'class': 'required form-control'}))
    amount = forms.IntegerField(widget=TextInput(attrs={'class': 'required form-control'}))
    purpose = forms.CharField(widget=TextInput(attrs={'class': 'required form-control'}))

