from mailqueue.models import MailerMessage  
from django.conf import settings
from django.contrib.auth.models import User
from users.models import NotificationSubject, Notification


def send_email(to_address,subject,content,html_content,bcc_address=settings.DEFAULT_BCC_EMAIL,attachment=None,attachment2=None,attachment3=None):
    new_message = MailerMessage()
    new_message.subject = subject
    new_message.to_address = to_address
    if bcc_address:
        new_message.bcc_address = bcc_address
    new_message.from_address = settings.DEFAULT_FROM_EMAIL
    new_message.content = content
    new_message.html_content = html_content
    if attachment:
        new_message.add_attachment(attachment)
    if attachment2:
        new_message.add_attachment(attachment2)
    if attachment3:
        new_message.add_attachment(attachment3)
    new_message.app = "DDU-GKY-Kudumashree Wayanad"
    new_message.save()
    
    
def get_name(user_id):
    name = User.objects.get(id=user_id).username
            
    return name


def get_email(user_id):
    email = User.objects.get(id=user_id).email            
    return email
            

def get_current_role(request):
    is_superadmin = False
    is_administrator = False
    is_playground_owner = False
    is_employer = False
    is_candidate = False
    is_student = False
    
    if request.user.is_authenticated():        
        
        if User.objects.filter(id=request.user.id,is_superuser=True,is_active=True).exists():
            is_superadmin = True
        
        if User.objects.filter(id=request.user.id,is_active=True,groups__name="administrator").exists():
            is_administrator = True
            
        if User.objects.filter(id=request.user.id,is_active=True,groups__name="playground_owner").exists():
            is_playground_owner = True
            
        if User.objects.filter(id=request.user.id,is_active=True,groups__name="employer").exists():
            is_employer = True

        if User.objects.filter(id=request.user.id,is_active=True,groups__name="candidate").exists():
            is_candidate = True

        if User.objects.filter(id=request.user.id,is_active=True,groups__name="student").exists():
            is_student = True
 
    current_role = "user"    
    
    if is_superadmin:
        current_role = "superadmin"
    elif is_administrator:
        current_role = "administrator"
    elif is_playground_owner:
        current_role = "playground_owner"
    elif is_employer:
        current_role = "employer"
    elif is_candidate:
        current_role = "candidate"
    elif is_student:
        current_role = "student"
    return current_role