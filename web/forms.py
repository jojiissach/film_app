from employers.models import Employer
from candidates.models import Candidate
from django import forms
from candidates.constants import PROFILE


class UploadForm(forms.ModelForm):
    class Meta:
        model = Employer
        fields = ('director_photo','cover_photo',)


class UploadPicForm(forms.ModelForm):
    class Meta:
        model = Candidate
        fields = ('profile_photo','cover_photo',)


class CandidateFilterForm(forms.Form):
    category = forms.ChoiceField(widget=forms.Select(attrs={'class':'select w-100-p',}),
                              choices=PROFILE)