from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator
from main.models import BaseModel


class PromotionVideo(BaseModel):
	video = models.ForeignKey("candidates.Video")
	promoted_date = models.DateField()
	end_date = models.DateField()
	is_deleted = models.BooleanField(default=False)
	is_active = models.BooleanField(default=False)
	class Meta:
		db_table = 'web_promotion_video'
		verbose_name = _('promotion video')
		verbose_name_plural = _('promotion videos')
		ordering = ('-date_added',)

	def __unicode__(self): 
		return self.promoted_date
