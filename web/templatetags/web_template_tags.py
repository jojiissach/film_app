from django.template import Library
register = Library()


@register.filter
def to_int(value):
    return int(value)