from django.conf.urls import url, include
import views


urlpatterns = [

    url(r'^$',views.home,name='home'),
    url(r'^about/$',views.about,name='about'),
    url(r'^contact/$',views.contact,name='contact'),
    url(r'^careers/$',views.careers,name='careers'),
    url(r'^employers/$',views.employers,name='employers'),
    url(r'^candidates/$',views.candidates,name='candidates'),
    url(r'^privacy/$',views.privacy,name='privacy'),
    url(r'^employer-profile/$',views.employer_profile,name='employer_profile'),
    url(r'^candidate-profile/$',views.candidate_profile,name='candidate_profile'),
    url(r'^student-profile/$',views.student_profile,name='student_profile'),
    url(r'^candidate/(?P<pk>.*)/$',views.candidate,name='candidate'),
    url(r'^upload-cover-photo/$', views.upload_cover, name='upload_cover'),
    url(r'^upload-candidate-photo/$', views.upload_candidate_cover, name='upload_candidate_cover'),
    url(r'^promote-video/(?P<pk>.*)/$',views.promote_video,name='promote_video'),
    url(r'^promoted-video/$',views.promoted_videos,name='promoted_videos'),  
    url(r'^promoted-video/delete/(?P<pk>.*)/$',views.delete,name='delete'), 
    url(r'^promoted-video/view/(?P<pk>.*)/$',views.promoted_video,name='promoted_video'), 

    url(r'^services/$',views.services,name='services'),
]
