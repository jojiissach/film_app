import datetime
from django.conf import settings
from employers.models import Employer
from jobs.models import JobVacancy
from candidates.models import Candidate, Video
from users.forms import LoginForm
from users.functions import get_current_role
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from web.models import PromotionVideo


def web_context(request):
    current_role = get_current_role(request)
    today = datetime.date.today()
    static_url = settings.STATIC_URL
    media_url = settings.MEDIA_URL
    companies_count = Employer.objects.filter(is_deleted=False,is_active=True).count()
    candidates_count = Candidate.objects.filter(is_deleted=False,is_active=True).count()
    vacancies_count = JobVacancy.objects.filter(is_active=True).count()
    login_form = LoginForm()
    promotion_videos = PromotionVideo.objects.filter(is_active=True)
    return {
        'web_title' : "Default Application",
        "static_url" : static_url,
        "media_url" : media_url,
        "today" : today,
        "companies_count" : companies_count,
        "candidates_count" : candidates_count,
        "vacancies_count" : vacancies_count,
        "confirm_job_apply" : "Are you sure to apply to this job.",
        "confirm_publish_message" : "Are you sure to publish this job.",
        "confirm_promotion_video" : "Are you sure you want to promote this video for 30 days",
        "login_form" : login_form,
        "promotion_videos" : promotion_videos,
        "quote1" : '"The casting is amazing, your dream become an actor."',
        "quote2" : "To light a candle is to cast a shadow."
     }
