from django.core.management.base import BaseCommand, CommandError
from jobs.models import JobVacancy
from datetime import date

class Command(BaseCommand):

    def handle(self, *args, **options):
        instances = JobVacancy.objects.filter(is_deleted=False,is_active=True)
        for instance in instances:
            last_date = instance.last_date
            if last_date:
                if date.today() > last_date:
                    instance.is_active = False
                    instance.save()