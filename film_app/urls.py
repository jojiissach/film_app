from django.conf.urls import url, include
from django.contrib import admin
from main import views as general_views
from django.conf import settings
from registration.backends.default.views import RegistrationView
from users.forms import RegForm
from users.backend import user_created
from django.views.static import serve
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views


urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^app/$',general_views.app,name='app'),
    url(r'^app/dashboard/$',general_views.dashboard,name='dashboard'),
    url(r'^verify/$',general_views.verify,name='verify'),
    url(r'^app/android/$',general_views.download_app,name='download_app'),
    
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^app/users-accounts/', include('users.urls', namespace="users")),
    url(r'^', include('web.urls', namespace="web")),
    url(r'^web-app/employers/', include('employers.urls', namespace="employers")),
    url(r'^web-app/candidates/', include('candidates.urls', namespace="candidates")),
    url(r'^web-app/jobs/', include('jobs.urls', namespace="jobs")),
    url(r'^payments/', include('instamojo.urls', namespace="instamojo")),

    url(r'^app/accounts/register/$', RegistrationView.as_view(form_class=RegForm),name='registration_register'),
    url(r'^zohoverify/verifyforzoho\.html$', TemplateView.as_view(template_name="web/verifyforzoho.html", content_type="text/html")),

    url(r'^media/(?P<path>.*)$', serve, { 'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve, { 'document_root': settings.STATIC_FILE_ROOT}),
    url('^', include('django.contrib.auth.urls')),
    url(r'^password_reset/$', auth_views.password_reset),
]
handler404 = 'web.views.handler404'
handler400 = 'web.views.handler400'
handler403 = 'web.views.handler403'
handler500 = 'web.views.handler500'