import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = '7pdm2lk814c3$)=m03rj4mhgqy^p5+#a0l^^_y!4t_wj$+338h'

DEBUG = True
ALLOWED_HOSTS = ['teneo.serveo.net','127.0.0.1','192.168.2.206','localhost'] 


INSTALLED_APPS = [
    'registration', 
    'mailqueue',
    'el_pagination',
    'versatileimagefield',
    'django_inlinecss',
    'dal',
    'dal_select2',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'main',
    'users',
    'web',
    'employers',
    'candidates',
    'jobs',
    'finance',
    # 'payumoney',
    "instamojo"
    
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'film_app.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'main.context_processors.main_context',
                'web.context_processors.web_context'
            ],
        },
    },
]

WSGI_APPLICATION = 'film_app.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'film_app',
        'USER': 'tegain',
        'PASSWORD': 'tegain',
        'HOST': 'localhost',
        'PORT': '',
    }
}
LOGIN_REDIRECT_URL = '/verify/'
LOGOUT_REDIRECT_URL = '/verify/'

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators
AUTHENTICATION_BACKENDS = (
    'users.backend.EmailOrUsernameModelBackend',
    'django.contrib.auth.backends.ModelBackend'
)

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


MAILQUEUE_LIMIT = 100
MAILQUEUE_QUEUE_UP = True

ACCOUNT_ACTIVATION_DAYS = 7
REGISTRATION_AUTO_LOGIN = True

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = False
USE_TZ = True

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.elasticemail.com'
EMAIL_HOST_USER = 'jojimon.tegain@gmail.com'
EMAIL_HOST_PASSWORD = 'cdb2d36d-75a2-4447-a768-f15817621908'
EMAIL_PORT = 2525
DEFAULT_FROM_EMAIL = 'jojimon.tegain@gmail.com'
DEFAULT_BCC_EMAIL = 'jojimon.tegain@gmail.com'
DEFAULT_REPLY_TO_EMAIL = 'jojimon.tegain@gmail.com'
SERVER_EMAIL = 'jojimon.tegain@gmail.com'
ADMIN_EMAIL = 'jojimon.tegain@gmail.com'

ENDLESS_PAGINATION_PER_PAGE = 20

MEDIA_URL = '/media/'
MEDIA_ROOT = '/srv/django/film_app/src/film_app/media/'
STATIC_URL = '/static/'
STATIC_FILE_ROOT = '/srv/django/film_app/src/film_app/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
    '/srv/django/film_app/src/film_app/static/',
)
