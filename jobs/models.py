from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _
from versatileimagefield.fields import VersatileImageField
from django.core.validators import MinValueValidator
from decimal import Decimal
from versatileimagefield.fields import VersatileImageField
from main.models import BaseModel
from django.contrib.postgres.fields import FloatRangeField
from candidates.constants import PROFILE
from users.functions import get_current_role


class JobVacancy(BaseModel):
    employer = models.ForeignKey("employers.Employer")
    job_category = models.CharField(max_length=128,choices=PROFILE)
    last_date = models.DateField()
    lead_actor = models.CharField(max_length=128,blank=True,null=True)
    lead_actress = models.CharField(max_length=128,blank=True,null=True)
    is_deleted = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    state = models.ForeignKey('candidates.State')
    class Meta:
        db_table = 'jobs_jobvacancy'
        verbose_name = _('jobvacancy')
        verbose_name_plural = _('jobvacancies')
        ordering = ('-date_added',)
  
    def __unicode__(self): 
        return self.job_category

    def apply(self):
        return JobInterest.objects.filter(vacancy=self).count()


class JobInterest(BaseModel):
    vacancy = models.ForeignKey("jobs.JobVacancy")
    candidate = models.ForeignKey("candidates.Candidate")
    interview = models.ForeignKey("jobs.Interview",blank=True,null=True)
    is_deleted = models.BooleanField(default=False)
    is_selected = models.BooleanField(default=False)
    is_completed = models.BooleanField(default=False)
    is_interviewed = models.BooleanField(default=False)
    class Meta:
        db_table = 'jobs_jobapply'
        verbose_name = _('job applied')
        verbose_name_plural = _('jobs applied')
        ordering = ('-date_added',)
  
    def __unicode__(self): 
        return self.vacancy.job_category


class Interview(BaseModel):
    vacancy = models.ForeignKey("jobs.JobVacancy")
    place = models.CharField(max_length=128)
    date = models.DateField()
    from_time = models.TimeField()
    to_time = models.TimeField()
    contact_number = models.CharField(max_length=15)
    second_contact_number = models.CharField(max_length=15)
    class Meta:
        db_table = 'jobs_interview'
        verbose_name = _('interview')
        verbose_name_plural = _('interviews')
        ordering = ('-date_added',)
  
    def __unicode__(self): 
        return self.vacancy.job_category
