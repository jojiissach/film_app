from django import forms
from django.forms.widgets import TextInput, Textarea, Select
from django.utils.translation import ugettext_lazy as _
from jobs.models import JobVacancy
from dal import autocomplete


form_class = "w-100-p t-a-c border-color-grey-400 bd-2 p-t-10 p-b-10 f-z-16"


class JobVacancyForm(forms.ModelForm):
    
    class Meta:
        model = JobVacancy
        exclude = ['creator','updator','auto_id','a_id','is_deleted','is_active','employer']
        widgets = {
            'job_category': Select(attrs={'class': 'w-100-p t-a-c border-color-grey-400 bd-2 p-t-10 p-b-10 f-z-16 select','data-placeholder':'Select Category'}),
            'last_date': TextInput(attrs={'class': 'w-100-p t-a-c border-color-grey-400 bd-2 p-t-10 p-b-10 f-z-16 datetimepicker','placeholder' : 'Last date','autocomplete':'off'}),
            'lead_actor': TextInput(attrs={'class': form_class,'placeholder' : 'Lead Actor','autocomplete':'off'}),
            'lead_actress': TextInput(attrs={'class': form_class,'placeholder' : 'Lead Actress','autocomplete':'off'}),
            'state': Select(attrs={'class': 'w-100-p t-a-c border-color-grey-400 p-t-10 p-b-10 f-z-16 select state-slect',}),
        }
        error_messages = {
            'job_category' : {
                'required' : _("Job Category field is required."),
            },
            'last_date' : {
                'required' : _("Last date field is required."),
            },
            'lead_actor' : {
                'required' : _("Actor field is required."),
            },
            'lead_actress' : {
                'required' : _("Actress field is required."),
            },
                      
        }

    def __init__(self, *args, **kwargs):
        super(JobVacancyForm, self).__init__(*args, **kwargs)
        self.fields['job_category'].empty_label = 'Select Category',

