from django.conf.urls import url, include
from django.contrib import admin
import views

urlpatterns = [ 
    
    url(r'^jobs/post-job/$',views.post_job,name='post_job'),
    url(r'^jobs/views/$',views.jobs,name='jobs'),
    url(r'^jobs/edit/(?P<pk>.*)/$',views.edit,name='edit'),
    url(r'^jobs/view/(?P<pk>.*)/$',views.job,name='job'),
    url(r'^jobs/delete/(?P<pk>.*)/$',views.delete,name='delete'),
    url(r'^jobs/delete-selected/$', views.delete_selected_jobs, name='delete_selected_jobs'),
    url(r'^jobs/apply/(?P<pk>.*)/$',views.apply,name='apply'),
    url(r'^jobs/candidate-applied-jobs/views/$',views.applied_jobs,name='applied_jobs'),
    url(r'^jobs/publish/(?P<pk>.*)/$',views.publish,name='publish'),
    url(r'^jobs/select/(?P<pk>.*)/$',views.select,name='select'),
    url(r'^jobs/unselect/(?P<pk>.*)/$',views.unselect,name='unselect'),

    url(r'^jobs/get-job-applied-list/$',views.get_job_applied_list,name='get_job_applied_list'),
    url(r'^jobs/get-job-selected-list/$',views.get_job_selected_list,name='get_job_selected_list'),
    url(r'^jobs/select-candidate/$',views.select_candidate,name='select_candidate'),
    url(r'^jobs/interview-call/(?P<pk>.*)/$',views.interview_call,name='interview_call'),
]