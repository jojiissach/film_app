


def as_json(request,ob):
    name = ob.candidate.name
    email = ob.candidate.email
    dob = str(ob.candidate.dob)
    place = str(ob.candidate.place)
    candidate = str(ob.candidate.pk)
    pk = str(ob.pk)
    if ob.is_selected:
        check = "true"
    else:
        check = "false"
    if ob.candidate.profile_photo:
        return dict(
            id=str(ob.id),
            name = name,
            email = email,
            dob = dob,
            place = place,
            pk = pk,
            candidate = candidate,
            check = check,
            image=ob.candidate.profile_photo.thumbnail['360x240'].url
        )
    else:
        return dict(
            id=str(ob.id),
            name = name,
            email = email,
            dob = dob,
            place = place,
            pk = pk,
            candidate = candidate,
            check = check,
        )