from django import forms
from django.forms.widgets import TextInput, Textarea, Select
from django.utils.translation import ugettext_lazy as _
from employers.models import Employer


form_class = 'w-100-p t-a-c border-color-grey-400 bd-2 p-t-10 p-b-10 f-z-16 p-l-10'


class EmployerForm(forms.ModelForm):
    
    class Meta:
        model = Employer
        exclude = ['creator','updator','auto_id','a_id','is_deleted','user','is_active',]
        widgets = {
            'banner_name': TextInput(attrs={'class': form_class,'placeholder' : 'Banner Name'}),
            'banner_no' : TextInput(attrs={'class': form_class,'placeholder' : 'Banner Registration No'}),
            'film_name': TextInput(attrs={'class': form_class,'placeholder' : 'Project Tittle'}),
            'director_name': TextInput(attrs={'class': form_class,'placeholder' : 'Director'}),
            'producer_name': TextInput(attrs={'class': form_class,'placeholder' : 'Producer'}),
            'time_duration': TextInput(attrs={'class': form_class,'placeholder' : 'Project Time Duration'}),
            'language': TextInput(attrs={'class': form_class,'placeholder' : 'Language'}),
            'email': TextInput(attrs={'class': form_class,'placeholder' : 'Email'}),

            'director_phone': TextInput(attrs={'class': form_class,'placeholder' : 'Director Phone'}),
            'director_wp': TextInput(attrs={'class': form_class,'placeholder' : 'Director Whatsapp No'}),
            'producer_phone': TextInput(attrs={'class': form_class,'placeholder' : 'Producer Phone'}),
            'producer_wp': TextInput(attrs={'class': form_class,'placeholder' : 'Producer Whatsapp No'}),
            'other_name' : TextInput(attrs={'class': form_class,'placeholder' : 'Other Name'}),
            'category' : Select(attrs={'class': 'w-100-p t-a-c border-color-grey-400 p-t-10 p-b-10 f-z-16 select','placeholder':'Category'}),
                                   
        }
        error_messages = {
            'banner_name' : {
                'required' : _("Banner Name field is required."),
            },
            'film_name' : {
                'required' : _("Film Name field is required."),
            },
            'director_name' : {
                'required' : _("Director Name field is required."),
            },
            'producer_name' : {
                'required' : _("Producer Name field is required."),
            },
            'time_duration' : {
                'required' : _("Duration field is required."),
            },
            'language' : {
                'required' : _("Language field is required."),
            },
            'email' : {
                'required' : _("E-mail field is required."),
            },
            'director_phone' : {
                'required' : _("Director Phone field is required."),
            },
            'producer_wp' : {
                'required' : _("Producer Whatsapp Number field is required."),
            },
            'other_name' : {
                'required' : _("Other Name field is required."),
            },
            'category' : {
                'required' : _("Category field is required."),
            }
        }
        def __init__(self, *args, **kwargs):
            super(CandidateForm, self).__init__(*args, **kwargs)
            self.fields['state'].empty_label = None
            