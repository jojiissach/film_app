from django.conf.urls import url, include
from django.contrib import admin
import views
from employers.views import EmployerAutocomplete

urlpatterns = [ 

	url(r'^employer-autocomplete/$',EmployerAutocomplete.as_view(),name='employer_autocomplete'),
    
    url(r'^employer/create/profile/$',views.create,name='create'),
    url(r'^employer/views/$',views.employers,name='employers'),
    url(r'^employer/edit/(?P<pk>.*)/$',views.edit,name='edit'),
    url(r'^employer/view/(?P<pk>.*)/$',views.employer,name='employer'),
    url(r'^employer/delete/(?P<pk>.*)/$',views.delete,name='delete'),
    url(r'^employer/delete-selected/$', views.delete_selected_employers, name='delete_selected_employers'),
    url(r'^employer/registration/(?P<pk>.*)/$',views.create_user,name='create_user')
]