from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _
from versatileimagefield.fields import VersatileImageField
from django.core.validators import MinValueValidator
from decimal import Decimal
from versatileimagefield.fields import VersatileImageField
from main.models import BaseModel
from candidates.constants import PROJECT_CATEGORY


class Employer(BaseModel):
    user = models.OneToOneField("auth.user",blank=True,null=True)
    banner_name = models.CharField(max_length=128)
    banner_no = models.CharField(max_length=128,blank=True,null=True)
    film_name = models.CharField(max_length=128)
    fefco_registration = VersatileImageField('Logo',upload_to="employers/fefco/",blank=True,null=True)
    director_name = models.CharField(max_length=128)    
    producer_name = models.CharField(max_length=128)
    time_duration = models.CharField(max_length=128)
    email = models.EmailField()
    language = models.CharField(max_length=128)
    is_deleted = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    director_photo = VersatileImageField('Director',upload_to="employers/director/",blank=True,null=True)
    cover_photo = VersatileImageField('Cover',upload_to="employers/cover/",blank=True,null=True)
    is_reg_fee_payed = models.BooleanField(default=False)

    director_phone = models.CharField(max_length=128,blank=True,null=True)
    director_wp = models.CharField(max_length=128,blank=True,null=True)
    other_name = models.CharField(max_length=12,blank=True,null=True)
    producer_wp = models.CharField(max_length=128,blank=True,null=True)
    producer_phone = models.CharField(max_length=128,blank=True,null=True)
    category = models.CharField(max_length=128,choices=PROJECT_CATEGORY)

    class Meta:
        db_table = 'employers_employer'
        verbose_name = _('employer')
        verbose_name_plural = _('employers')
        ordering = ('banner_name',)
  
    def __unicode__(self): 
        return self.banner_name