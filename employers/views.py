from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse, HttpResponseRedirect
import json
from main.decorators import check_mode,ajax_required
from main.functions import generate_form_errors, get_auto_id, get_a_id
import datetime
from django.db.models import Q
from django.views.decorators.http import require_GET
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory
from django.core import serializers
from decimal import Decimal
from django.views.decorators.http import require_POST
from django.template.loader import render_to_string
from employers.models import Employer
from employers.forms import EmployerForm
from django.contrib.auth.models import User, Group
from users.forms import RegistrationForm
import random, string
from users.functions import send_email
from dal import autocomplete
from jobs.models import JobVacancy, JobInterest
from django.contrib.auth import authenticate 
from django.contrib.auth import login as auth_login


class EmployerAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = Employer.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(phone__istartswith=self.q) | 
                                 Q(name__istartswith=self.q)
                                )
    
        return items


@check_mode
def create(request):    
    
    if request.method == 'POST':
        form = EmployerForm(request.POST,request.FILES)
        
        if form.is_valid() : 
            
            auto_id = get_auto_id(Employer)
            a_id = get_a_id(Employer,request)
            #create product
            data = form.save(commit=False)
            data.auto_id = auto_id
            data.a_id = a_id 
            data.is_active = True
            data.is_reg_fee_payed = True
            data.save()
            
            return HttpResponseRedirect(reverse('employers:create_user',kwargs={'pk':data.pk}))  
        
        else:
            context = {
                "title" : "Create Shop",
                "form" : form,
                "url" : reverse('employers:create'),
                "is_need_select" : True,
            }
            return render(request,'employers/entry.html',context)
    
    else:       
        form = EmployerForm()
        
        context = {
            "title" : "Profile",
            "form" : form,
            "url" : reverse('employers:create'),
            "redirect" : True,
            "is_need_select" : True,
        }
        return render(request,'employers/entry.html',context)


@check_mode
def employers(request):
    instances = Employer.objects.filter(is_deleted=False,is_active=True)
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(auto_id__icontains=query) | Q(banner_name__icontains=query) | Q(film_name__icontains=query) | Q(director_name__icontains=query) | Q(category__icontains=query))
        title = "Employers - %s" %query 
    context = {
        "instances" : instances,
        'title' : "Companies",

        "is_need_custom_scroll_bar" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_wave_effect" : True,
        "is_need_popup_box" : True,
    }
    return render(request,'employers/employers.html',context) 


@check_mode
def employer(request,pk):
    instance = get_object_or_404(Employer.objects.filter(pk=pk,is_deleted=False))
    context = {
        "instance" : instance,
        "title" : instance.banner_name,

        "is_need_custom_scroll_bar" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_wave_effect" : True,
        "is_need_popup_box" : True,
    }
    return render(request,'employers/employer.html',context)


@check_mode
def edit(request,pk):
    instance = get_object_or_404(Employer.objects.filter(pk=pk,is_deleted=False)) 
    is_payed = instance.is_reg_fee_payed
    if request.method == 'POST':
        response_data = {}
        form = EmployerForm(request.POST,request.FILES,instance=instance)
        
        if form.is_valid() : 
            data = form.save(commit=False)
            data.is_reg_fee_payed = is_payed 
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save() 

            return HttpResponseRedirect(reverse('web:employer_profile',))  
        
        else:       
            message = generate_form_errors(form,formset=False) 
            context = {
                "title" : "Create Shop",
                "form" : form,
                "url" : reverse('employers:edit',kwargs={'pk':instance.pk}),
                "message" : message,
                "is_need_select" : True,
            }
            return render(request,'employers/entry.html',context)
    else:

        form = EmployerForm(instance=instance)
        context = {
            "form" : form,
            "title" : "Edit Profile",
            "instance" : instance,
            "url" : reverse('employers:edit',kwargs={'pk':instance.pk}),
            "is_need_select" : True,
        }
        return render(request, 'employers/entry.html', context)


@check_mode
@ajax_required
def delete(request,pk):
    instance = get_object_or_404(Employer.objects.filter(pk=pk,is_deleted=False))
    Employer.objects.filter(pk=pk).update(is_deleted=True,is_active=False,banner_name=instance.banner_name + "_deleted_" + str(instance.auto_id))
    if User.objects.filter(id=instance.user.id).exists():
        user = User.objects.get(id=instance.user.id)
        User.objects.filter(id=instance.user.id).delete()
    JobVacancy.objects.filter(is_deleted=False,employer=instance).update(is_deleted=True,is_active=False)
    JobInterest.objects.filter(is_deleted=False,vacancy__employer=instance).update(is_deleted=True,is_selected=False,is_completed=False,is_interviewed=False)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Employer Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('employers:employers')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
def delete_selected_employers(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Employer.objects.filter(pk=pk,is_deleted=False)) 
            Employer.objects.filter(pk=pk).update(is_deleted=True,banner_name=instance.banner_name + "_deleted_" + str(instance.auto_id))
            
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Employer(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('employers:employers')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
def create_user(request,pk): 
    employer = get_object_or_404(Employer.objects.filter(pk=pk))

    if request.method == "POST":
        form = RegistrationForm(request.POST)
        response_data = {} 
        message = ""  
        data = [] 
        username = request.POST.get('username')
        password = request.POST.get('password1')
        emails = request.POST.get('email')
        email = str(emails)
        error = False
        if User.objects.filter(email=email).exists():
            error = True
            message += "This email already exists."
        if employer.user:
            error = True
            message += "This candidate already has a user."
    
        if not error:
            if form.is_valid():    

                data = form.save(commit=False)
                data.email = email
                data.is_active = True
                data.save()

                instance = data
                group = Group.objects.get(name="employer")
                instance.groups.add(group)

                employer.user=data
                employer.save()
                user = authenticate(username=username, password=password)
                auth_login(request, user)
                # return HttpResponseRedirect(reverse('instamojo:payment')+"?registration=True")
                return HttpResponseRedirect(reverse('web:employer_profile'))
    
            else: 
                context = {
                    "title" : "Registration",
                    "form" : form,
                    "url" : reverse('employers:create_user',kwargs={'pk':pk})
                }
                return render(request,'registration/registration.html',context)
    
        else:      
            context = {
                "title" : "Registration",
                "form" : form,
                "url" : reverse('employers:create_user',kwargs={'pk':pk})
            }
            return render(request,'registration/registration.html',context)

    else: 
        form = RegistrationForm(initial={'email' : employer.email})
        
        context = {
            "form" : form,
            "title" : "Registration",
            "redirect" : True,
        }
    return render(request, 'registration/registration.html', context)