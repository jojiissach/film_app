GENDER = (
	(u'', u'Select gender'),
    ('male', 'Male'),
    ('female', 'Female'),
    ('other', 'Other'), 
)

PROFILE = (
	(u'', u'Select category'),
	('actor','Actor'),
	('actress','Actress'),
	('anchor','Anchor'),
	('supporting_actor','Supporting/Junior Actor'),
	('models','Models'),
	('associate_director','Associate Director'),
	('assistant_director','Assistant Director'),
	('camera_man','Camera Man'),
	('camera_associate','Camera Associate'),
	('editor','Editor'),
	('controller','Controller'),
	('spot_editor','Spot Editor'),
	('music_director','Music Director'),
	('singer','Singer'),
	('makeup','Makeup'),
	('costumer','Costumer'),
	('designer','Designer'),
	('choreographer','Choreographer'),
	('item_dancer','Item Dancer'),
	('professional_dancer','Professional Dancer'),
	
)
PROJECT_CATEGORY = (
	(u'', u'Select category'),
	('film','Film'),
	('short_film','Short Film'),
	('album','Album'),
	('documentary','Documentary'),
	('tele_film','Tele Film'),
	('drama','Drama'),
)