from django.conf.urls import url, include
from django.contrib import admin
import views

urlpatterns = [ 
    
    url(r'^candidate/create/profile/$',views.create,name='create'),
    url(r'^candidate/views/$',views.candidates,name='candidates'),
    url(r'^candidate/edit/(?P<pk>.*)/$',views.edit,name='edit'),
    url(r'^candidate/view/(?P<pk>.*)/$',views.candidate,name='candidate'),
    url(r'^candidate/delete/(?P<pk>.*)/$',views.delete,name='delete'),
    url(r'^candidate/delete-selected/$', views.delete_selected_candidates, name='delete_selected_candidates'),
    url(r'^candidate/registration/(?P<pk>.*)/$',views.create_user,name='create_user'),
    url(r'^account-verify/(?P<pk>.*)/$',views.account_verify,name='account_verify'),
    url(r'^applied-candidates/view/(?P<pk>.*)/$',views.applied_candidates,name='applied_candidates'),
]