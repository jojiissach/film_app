from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse, HttpResponseRedirect
import json
from main.decorators import check_mode,ajax_required
from main.functions import generate_form_errors, get_auto_id, get_a_id
import datetime
from django.db.models import Q
from django.views.decorators.http import require_GET
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory
from django.core import serializers
from decimal import Decimal
from django.views.decorators.http import require_POST
from django.template.loader import render_to_string
from candidates.models import Candidate, Experience, Education, Video
from candidates.forms import CandidateForm
from django.contrib.auth.models import User, Group
from users.forms import RegistrationForm
import random, string
from users.functions import send_email
from django.contrib.auth import authenticate 
from django.contrib.auth import login as auth_login


@check_mode
def create(request):    
    
    if request.method == 'POST':
        form = CandidateForm(request.POST,request.FILES)
        if form.is_valid() : 
            
            auto_id = get_auto_id(Candidate)
            a_id = get_a_id(Candidate,request)
            #create product
            name = form.cleaned_data['name']

            data = form.save(commit=False)
            profile = request.POST['profile'] 
            data.auto_id = auto_id
            data.a_id = a_id
            data.name = name.capitalize() 
            data.profile = profile
            data.save()

            return HttpResponseRedirect(reverse('candidates:create_user',kwargs={'pk':data.pk}))
        
        else:
            message = generate_form_errors(form,formset=False)
            print form.errors
            context = {
                "title" : "Registration",
                "form" : form,
                "url" : reverse('candidates:create'),
                "redirect" : True,
                "is_need_select" : True,
                "is_need_datepicker" : True,
                "is_need_steps" : True,
            }
            return render(request,'candidates/entry.html',context)
    
    else:       
        form = CandidateForm()
        
        context = {
            "title" : "Registration",
            "form" : form,
            "url" : reverse('candidates:create'),
            "redirect" : True,
            "is_need_select" : True,
            "is_need_datepicker" : True,
            "is_need_steps" : True,
        }
        return render(request,'candidates/entry.html',context)


@check_mode
def candidates(request):
    instances = Candidate.objects.filter(is_deleted=False,is_active=True)
    query = request.GET.get("q")

    if query:
        instances = instances.filter(Q(auto_id__icontains=query) | Q(name__icontains=query) | Q(profile__icontains=query) | Q(gender__icontains=query) | Q(state__name__icontains=query))
        title = "Candidates - %s" %query  
    
    context = {
        "instances" : instances,
        'title' : "Candidates",

        "is_need_custom_scroll_bar" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_wave_effect" : True,
        "is_need_popup_box" : True,
    }
    return render(request,'candidates/candidates.html',context)


@check_mode
def applied_candidates(request,pk):
    instances = Candidate.objects.filter(is_deleted=False,jobapply__vacancy__pk=pk)
    query = request.GET.get("q")
    
    context = {
        "instances" : instances,
        'title' : "Applied Candidates",

        "is_need_custom_scroll_bar" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_wave_effect" : True,
        "is_need_popup_box" : True,
    }
    return render(request,'web/candidate/candidates.html',context) 


@check_mode
def candidate(request,pk):
    instance = get_object_or_404(Candidate.objects.filter(pk=pk,is_deleted=False))
    title = instance.name
    context = {
        "instance" : instance,
        "title" : title,

        "is_need_custom_scroll_bar" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_wave_effect" : True,
        "is_need_popup_box" : True,
    }
    return render(request,'candidates/candidate.html',context)


@check_mode
def edit(request,pk):
    instance = get_object_or_404(Candidate.objects.filter(pk=pk,is_deleted=False)) 
    is_payed = instance.is_reg_fee_payed
    if request.method == 'POST':
        response_data = {}
        form = CandidateForm(request.POST,request.FILES,instance=instance)
        
        if form.is_valid() :  
            educations = request.POST.getlist('education')
            experiences = request.POST.getlist('experience')
            video_urls = request.POST.getlist('video_url')
            name = form.cleaned_data['name']
            data = form.save(commit=False)
            data.updator = request.user
            data.is_reg_fee_payed = is_payed
            data.date_updated = datetime.datetime.now()
            data.name = name.capitalize() 
            data.save() 
            # Education.objects.filter(candidate=data).delete()
            # Experience.objects.filter(candidate=data).delete()
            # Video.objects.filter(candidate=data).delete()
            for education in educations:
                if not education =='':
                    Education.objects.create(
                            candidate = data,
                            name = education
                        )

            for experience in experiences:
                if not experience =='':
                    Experience.objects.create(
                            candidate = data,
                            experience = experience
                        )

            for video_url in video_urls:
                if not video_url =='':
                    urls = video_url.split('/')
                    code = urls[-1]
                    if code == '':
                        code = urls[-2]
                    Video.objects.create(
                            candidate = data,
                            video_url = video_url,
                            video_code = code
                        )
            response_data = {
                "status": 'true',
                "redirect_url" : reverse('web:candidate_profile')
            }
            return HttpResponse(json.dumps(response_data), content_type='application/javascript')
        
        else:
            message = generate_form_errors(form,formset=False)
            response_data = {
                "status": 'false',
                "message" : message
            }
            return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:

        form = CandidateForm(instance=instance)
        context = {
            "form" : form,
            "is_edit" : True,
            "title" : "Edit Profile",
            "instance" : instance,
            "url" : reverse('candidates:edit',kwargs={'pk':instance.pk}),
            "is_need_select" : True,
            "is_need_datepicker" : True,
            "is_need_steps" : True,
        }
        return render(request, 'candidates/edit.html', context)


@check_mode
@ajax_required
def delete(request,pk):
    instance = get_object_or_404(Candidate.objects.filter(pk=pk,is_deleted=False))
    Candidate.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Candidate Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('candidates:candidates')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
def delete_selected_candidates(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Candidate.objects.filter(pk=pk,is_deleted=False)) 
            Candidate.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
            
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Candidate(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('candidates:candidates')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
def create_user(request,pk): 
    candidate = get_object_or_404(Candidate.objects.filter(pk=pk))

    if request.method == "POST":
        form = RegistrationForm(request.POST)
        response_data = {} 
        message = ""  
        data = []  
        emails = request.POST.get('email')
        username = request.POST.get('username')
        password = request.POST.get('password1')
        email = str(emails)
        error = False
        if User.objects.filter(email=email).exists():
            error = True
            message += "This email already exists."
        if candidate.user:
            error = True
            message += "This candidate already has a user."
    
        if not error:
            if form.is_valid():    
                data = form.save(commit=False)
                data.email = email
                data.is_active = True
                data.save()

                instance = data
                group = Group.objects.get(name="candidate")
                instance.groups.add(group)

                candidate.user=data
                candidate.save()
                user = authenticate(username=username, password=password)
                auth_login(request, user)
                return HttpResponseRedirect(reverse('instamojo:payment')+"?registration=True") 
    
            else:          
                context = {
                    "title" : "Registration",
                    "form" : form,
                    "url" : reverse('candidates:create_user',kwargs={'pk':pk})
                }
                return render(request,'registration/registration.html',context)
    
        else:           
            context = {
                "title" : "Registration",
                "form" : form,
                "message" : message,
                "url" : reverse('candidates:create_user',kwargs={'pk':pk})
            }
            return render(request,'registration/registration.html',context)

    else: 
        form = RegistrationForm(initial={'email' : candidate.email})
        
        context = {
            "form" : form,
            "title" : "Registration",
            "redirect" : True,
        }
    return render(request, 'registration/registration.html', context)



def account_verify(request,pk):
    if User.objects.filter(is_active=False,pk=pk).exists():
        user = get_object_or_404(User.objects.filter(is_active=False,pk=pk))

        if user:
            User.objects.filter(pk=pk).update(is_active=True)
            Candidate.objects.filter(user=user).update(is_active=True)

            context = {
            'title' : 'Activation Complete',
            }
            return render(request,"registration/candidate_activation_complete.html",context)
        else:

            return HttpResponseRedirect(reverse('web:home',))
    else:
        return HttpResponseRedirect(reverse('web:home',))
