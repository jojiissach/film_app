from django import forms
from django.forms.widgets import TextInput, Textarea, Select
from django.utils.translation import ugettext_lazy as _
from candidates.models import Candidate, Education, Experience


form_class = 'w-100-p t-a-c border-color-grey-400 p-t-10 p-b-10 f-z-16'

class CandidateForm(forms.ModelForm):
    
    class Meta:
        model = Candidate
        exclude = ['creator','updator','auto_id','a_id','is_deleted','user','is_active','profile']
        widgets = {
            'name': TextInput(attrs={'class': form_class,'placeholder' : 'Name'}),
            'dob': TextInput(attrs={'class': "w-100-p t-a-c border-color-grey-400  p-t-10 p-b-10 f-z-16 datetimepicker",'placeholder' : 'Date of Birth','autocomplete':'off'}),
            'gender': Select(attrs={'class': 'w-100-p t-a-c border-color-grey-400 p-t-10 p-b-10 f-z-16 select','placeholder':'Gender'}),
            'mother_tounge': TextInput(attrs={'class': form_class,'placeholder' : 'Mother Tounge'}),
            'mobile_number': TextInput(attrs={'class': form_class,'placeholder' : 'Mobile Number'}),
            'email': TextInput(attrs={'class': form_class,'placeholder' : 'Email'}),
            'place': TextInput(attrs={'class': form_class,'placeholder' : 'Place'}),
            'state': Select(attrs={'class': 'w-100-p t-a-c border-color-grey-400 p-t-10 p-b-10 f-z-16 select state-slect',}),
            'pin': TextInput(attrs={'class': form_class,'placeholder' : 'PIN'}),

            'height': TextInput(attrs={'class': form_class,'placeholder' : 'Height (Foot)'}),
            'address': TextInput(attrs={'class': form_class,'placeholder' : 'Address'}),
            'weight': TextInput(attrs={'class': form_class,'placeholder' : 'Weight (Kg)'}),
            'body_type': TextInput(attrs={'class': form_class,'placeholder' : 'Body Type'}),
            'skin_tone': TextInput(attrs={'class': form_class,'placeholder' : 'Skin Type'}),
            'additional_info': TextInput(attrs={'class': form_class,'placeholder' : 'Additional Info'}),

            'work_attended': Textarea(attrs={'class': form_class,'placeholder' : 'Add any drama/theater work you attended'}),
            'media_related_studies': Textarea(attrs={'class': form_class,'placeholder' : 'Add any media related studies'}),

            'about': Textarea(attrs={'class': form_class,'placeholder' : 'Tell me something about you'}),
            'dreams': Textarea(attrs={'class': form_class,'placeholder' : 'Tell me something about your dreams'}),
            'media_experience': Textarea(attrs={'class': form_class,'placeholder' : 'Tell me something about your media related experience(if have experience)'}),
            'passion_extra_skills': Textarea(attrs={'class': form_class,'placeholder' : 'Tell me something about your passion and extra skills'}),
                 
        }
        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
            'email' : {
                'required' : _("Email field is required."),
            },
            'phone' : {
                'required' : _("Phone field is required."),
            },
            'address' : {
                'required' : _("Address field is required."),
            },
            'place' : {
                'required' : _("Place field is required."),
            },
            'gender' : {
                'required' : _("Gender field is required."),
            },
            'profile_photo' : {
                'required' : _("Profile Photo field is required."),
            },
            
        }
        def __init__(self, *args, **kwargs):
            super(CandidateForm, self).__init__(*args, **kwargs)
            self.fields['state'].empty_label = None


class EducationForm(forms.ModelForm):
    
    class Meta:
        model = Education
        exclude = ['candidate']
        widgets = {
            'name': TextInput(attrs={'class': form_class,'placeholder' : 'Name'}),
                 
        }
        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
            
        }


class ExperienceForm(forms.ModelForm):
    
    class Meta:
        model = Experience
        exclude = ['candidate']
        widgets = {
            'name': TextInput(attrs={'class': form_class,'placeholder' : 'Add any experience in your selected profile'}),
                 
        }
        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
            
        }