from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.contrib import messages
import logging, traceback
import payumoney.constants as constants
import payumoney.config as config
import hashlib
from random import randint
from django.views.decorators.csrf import csrf_exempt
from finance.models import BankAccount
from users.functions import get_current_role
from candidates.models import Candidate
from employers.models import Employer
from main.functions import get_auto_id, get_a_id
from payumoney.models import Payment
from main.decorators import check_mode
import datetime
from django.shortcuts import render, get_object_or_404
from web.models import PromotionVideo
from django.contrib.auth.models import User


@check_mode
@login_required
def payment(request):
    current_role = get_current_role(request)
    registration = request.GET.get('registration')
    promotion = request.GET.get('promotion')
    video = request.GET.get('video')
    is_registration = False
    is_promotion = False
    if registration:
        if registration == "True":
            is_registration = True
            amount_to_pay = constants.REGISTRATION_FEE
    if promotion:
        if promotion == "True":
            is_promotion = True
            amount_to_pay = constants.VIDEO_PROMOTION_FEE

    if current_role == 'candidate':
        customer = Candidate.objects.get(user=request.user)
        name = customer.name
        phone = customer.mobile_number
        address = customer.address
    else:
        customer = Employer.objects.get(user=request.user)
        name = customer.banner_name
        phone = ''
        address = customer.director_name+" "+customer.banner_name

    email = request.user.email

    if Candidate.objects.filter(user=request.user,is_active=True,is_reg_fee_payed=True):
        true_user = True
    elif Employer.objects.filter(user=request.user,is_active=True,is_reg_fee_payed=True):
        true_user = True
    else :
        true_user = False
    
    data = {}
    txnid = get_transaction_id()
    hash_ = generate_hash(request, txnid, amount_to_pay ,name)
    hash_string = get_hash_string(request, txnid, amount_to_pay, name)
    # use constants file to store constant values.
    # use test URL for testing
    data["action"] = constants.PAYMENT_URL_LIVE 
    data["amount"] = amount_to_pay
    data["productinfo"]  = constants.PAID_FEE_PRODUCT_INFO
    data["key"] = config.KEY
    data["txnid"] = txnid
    data["hash"] = hash_
    data["hash_string"] = hash_string
    data["firstname"] = name
    data["email"] = email
    data["phone"] = phone
    data["address"] = address
    data["zipcode"] = "asd"
    data["city"] = "ad"
    data["state"] = "ads"
    data["service_provider"] = constants.SERVICE_PROVIDER
    #transaction creation payumoney
    currency = 'INR'
    language = 'EN'
    auto_id = get_auto_id(Payment)
    a_id = get_a_id(Payment,request)

    data['true_user'] = true_user
    #create pending payment
    payment = Payment.objects.create(
        creator=request.user,
        updator=request.user,
        amount=amount_to_pay,
        auto_id=auto_id,
        a_id = a_id,
        order_status="Pending/Rejected",
        currency=currency,
        language=language,
        payment_mode="payu_paisa",
        is_registration = is_registration,
        is_promotion = is_promotion
    )   
    #end here
    data["furl"] = request.build_absolute_uri(reverse("payumoney:payment_failure")+"?payment="+str(payment.pk)+"&video="+str(video))
    data["surl"] = request.build_absolute_uri(reverse("payumoney:payment_success")+"?payment="+str(payment.pk)+"&video="+str(video))

    return render(request, "payumoney/online_payment.html", data)        
    
# generate the hash
def generate_hash(request, txnid,amount,name):
    try:
        # get keys and SALT from dashboard once account is created.
        # hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10"
        hash_string = get_hash_string(request,txnid,amount,name)
        generated_hash = hashlib.sha512(hash_string.encode('utf-8')).hexdigest().lower()
        return generated_hash
    except Exception as e:
        # log the error here.
        logging.getLogger("error_logger").error(traceback.format_exc())
        return None

# create hash string using all the fields
def get_hash_string(request, txnid, amount,name):

    hash_string = config.KEY+"|"+txnid+"|"+str(amount)+"|"+constants.PAID_FEE_PRODUCT_INFO+"|"
    hash_string += name+"|"+request.user.email+"|"
    hash_string += "||||||||||"+config.SALT
    return hash_string

# generate a random transaction Id.
def get_transaction_id():
    hash_object = hashlib.sha256(str(randint(0,9999)).encode("utf-8"))
    # take approprite length
    txnid = hash_object.hexdigest().lower()[0:32]
    return txnid


@csrf_exempt
def payment_success(request):
    current_role = get_current_role(request)
    pk = request.GET.get('payment')
    firstname = request.POST.get('firstname')
    address1 = request.POST.get('address1')
    city = request.POST.get('city')
    state = request.POST.get('state')
    zipcode = request.POST.get('zipcode')
    email = request.POST.get('email')
    phone = request.POST.get('phone')
    mode = request.POST.get('mode')
    name_on_card = request.POST.get('name_on_card')
    txnid = request.POST.get('txnid')
    bank_ref_num = request.POST.get('bank_ref_num')
    bankcode = request.POST.get('bankcode')
    payuMoneyId = request.POST.get('payuMoneyId')
    mihpayid = request.POST.get('mihpayid')
    status = request.POST.get('status')
    error_Message = request.POST.get('error_Message')

    payment_obj = None
    if Payment.objects.filter(pk=pk).exists():
        payment_obj = Payment.objects.get(pk=pk)

    if payment_obj:
        #checking the payment is for registration
        if payment_obj.is_registration:
            if current_role == 'candidate':
                candidate = Candidate.objects.get(user=request.user)
                candidate.is_reg_fee_payed = True
                candidate.is_active = True
                candidate.save()
            else:
                employer = Employer.objects.get(user=request.user)
                employer.is_reg_fee_payed = True
                employer.is_active = True
                employer.save()
        #checking th epayment is for promotion
        if payment_obj.is_promotion:
            end_date = datetime.date.today() + datetime.timedelta(30)
            video  = request.GET.get('video')
            if video:
                instance = get_object_or_404(PromotionVideo.objects.filter(pk=video))
                instance.is_active = True
                instance.end_date = end_date
                instance.save()
        #payment checking end here
        payment_obj.billing_name = firstname
        payment_obj.billing_address = address1
        payment_obj.billing_state = state
        payment_obj.billing_city = city
        payment_obj.billing_zip = zipcode
        payment_obj.billing_city = city
        payment_obj.billing_email = email
        payment_obj.billing_tel = phone
        payment_obj.order_status = status

        payment_obj.delivery_name = firstname
        payment_obj.delivery_address = address1
        payment_obj.delivery_state = state
        payment_obj.delivery_city = city
        payment_obj.delivery_zip = zipcode
        payment_obj.delivery_tel = phone
        payment_obj.delivery_email = email

        payment_obj.payment_type = mode
        payment_obj.card_name = name_on_card
        payment_obj.transaction_id = txnid
        payment_obj.bank_ref_no = bank_ref_num
        payment_obj.bank_id = bankcode
        payment_obj.payuid = payuMoneyId
        payment_obj.mihpayid = mihpayid
        payment_obj.status_message = status
        payment_obj.failure_message = error_Message

        payment_obj.is_completed = True

        payment_obj.save()

        receiver_email = "jojimon.tegain@gmail.com"
        if BankAccount.objects.filter(is_deleted=False,payumoney_email=receiver_email).exists():
            bank_account = BankAccount.objects.get(is_deleted=False,payumoney_email=receiver_email)
            balance = bank_account.balance
            bank_account.balance = balance + Decimal(amount)
            bank_account.save()
            payment_obj.bank_account = bank_account
            payment_obj.save()
   
    return HttpResponseRedirect(reverse('web:home'))


@csrf_exempt
def payment_failure(request):
    order = request.GET.get('order')
    pk = request.GET.get('order')
    order = None
    if pk:
        if EcommerceOrder.objects.filter(pk=pk).exists():
            order = EcommerceOrder.objects.get(pk=pk)

    if order:
        order.payment_status = "cancelled"
        order.status = "cancelled"
        order.save()

        if order.payment:
            order.payment.order_status = "Cancelled"
            order.payment.save()   

    if Candidate.objects.filter(user=request.user,is_active=True,is_reg_fee_payed=True):
        true_user = True
    elif Candidate.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False):
        true_user = False
        candidate = Candidate.objects.get(user=request.user,is_active=False,is_reg_fee_payed=False)
        Candidate.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).delete()
        User.objects.filter(username=candidate.user.username,email=candidate.user.email).delete()
    elif Employer.objects.filter(user=request.user,is_active=True,is_reg_fee_payed=True):        
        true_user = True
    elif Employer.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False):
        true_user = False
        employer = Employer.objects.get(user=request.user,is_active=False,is_reg_fee_payed=False)
        Employer.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).delete()
        User.objects.filter(username=candidate.user.username,email=candidate.user.email).delete()
    else :
        true_user = False
    data = {
        "true_user" : true_user,
    }

    return render(request, "payumoney/failure.html", data)

    
@csrf_exempt
def payment_cancel(request):  
    if Candidate.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).exists():
        candidate = Candidate.objects.get(user=request.user,is_active=False,is_reg_fee_payed=False)
        Candidate.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).delete()
        User.objects.filter(username=candidate.user.username,email=candidate.user.email).delete()
    elif Employer.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).exists(): 
        employer = Employer.objects.get(user=request.user,is_active=False,is_reg_fee_payed=False)       
        Employer.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).delete()        
        User.objects.filter(username=employer.user.username,email=employer.user.email).delete()

    data = {
        "true_user" : False,
    }

    return render(request, "payumoney/failure.html", data)