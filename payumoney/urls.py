from django.conf.urls import url
from payumoney import views

app_name = "appname"
urlpatterns = [
    url(r'^payment/$', views.payment, name="payment"),
    url(r'^payment/success/$', views.payment_success, name="payment_success"),
    url(r'^payment/failure/$', views.payment_failure, name="payment_failure"),
    url(r'^payment/cancel/$', views.payment_cancel, name="payment_cancel"),
]